Consider a `cars` table. History is tracked in `hist_cars`. Below is how such a table might evolve

**Original data**

cars

| car_id	|model	|age|
|----|----|----|
|1	    |Ford	|6|
|2	    |Jeep	|3|
|3	    |Audi	|2|

hist_cars

|hist_id |car_id	|model	|age|
|----|----|----|----|
|1       |1	    |Ford	|6|
|2       |2	    |Jeep	|3|
|3       |3	    |Audi	|2|



**Update row 1, change age to 1**

cars

| car_id	|model	|age|
|----|----|----|
|1	    |Ford	|1
|2	    |Jeep	|3
|3	    |Audi	|2


hist_cars

|hist_id |car_id	|model	|age|
|----|----|----|----|
|1       |1	    |Ford   |6
|2       |2	    |Jeep	|3
|3       |3	    |Audi	|2
|4       |1     |Ford   |1

**Add color column to table**

cars

| car_id	|model	|age|color
|----|----|----|----|
|1	    |Ford	|1   |red
|2	    |Jeep	|3   |blue
|3	    |Audi	|2   |white

hist_cars

|hist_id |car_id	|model	|age|color
|----|----|----|----|---
|1       |1	    |Ford	|6   |null
|2       |2	    |Jeep	|3   |null
|3       |3	    |Audi   |2   |null
|4       |1     |Ford   |1   |null
|5       |1	    |Ford	|1   |red
|6       |2	    |Jeep	|3   |blue
|7       |3     |Audi    |2   |white
